﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace Book_Parallel_Assing
{
    class Program
    {
        public static void SomeAdd(int i, int y)
        {
            Console.WriteLine("--------------------------------");
            PrintCurrentThread();
            Console.WriteLine($"{i + y}");
            Thread.Sleep(2000);
            Console.WriteLine("End");
        }
        public delegate void Add(int x, int y);
        static void Main(string[] args)
        {
            var mutext = new Mutex(false, "some");

            var isLocket = mutext.WaitOne(0);
            if (!isLocket)
            {
                Console.WriteLine($"Some other process user this mutext {"some"}");
                return;
            }

            //PrintCurrentThread();
            var timer = new Stopwatch();
            timer.Start();
            var list = new List<int> { 1, 22, 33, 44, 55, 66, 77, 88, 99 };
            var result2 = list.AsParallel().Select(x => DoWorkd(x)).ToList();
            timer.Stop();
            Console.WriteLine($"Spend time: {timer.ElapsedMilliseconds}");
            foreach (var item in result2)
            {
                Console.WriteLine(item);
            }

            mutext.ReleaseMutex();
            return;
            var delegat = new Add(SomeAdd);
            delegat(3, 4);
            delegat.Invoke(3, 5);
        }

        private static readonly object someObject = new object();
        private static readonly Semaphore semaphore = new Semaphore(2, 2);

        public static string DoWorkd(int i)
        {
            Console.WriteLine($"-> Start thred with Id: {Thread.CurrentThread.ManagedThreadId}");

            semaphore.WaitOne();

            //Monitor.Enter(someObject);
            //lock (someObject)
            {
                Console.WriteLine($"-> i: {i}, ManagedThreadId: {Thread.CurrentThread.ManagedThreadId}");
                Thread.Sleep(2000);
                Console.WriteLine($"-> End thred with Id: {Thread.CurrentThread.ManagedThreadId}");
            }
            //Monitor.Exit(someObject);
            semaphore.Release();
            return i.ToString();
        }

        public static void PrintCurrentThread()
        {
            Console.WriteLine($"ManagedThreadId: {Thread.CurrentThread.ManagedThreadId}");
        }
    }
}
